var request = require('request');
var parseString = require('xml2js').parseString;

module.exports = function(searchTerm, callback){
	var url = "http://api.wolframalpha.com/v2/query?" + 
		"appid=" + "2Q7R4W-5WRQT7LVH8" + 
		"&input=" + encodeURIComponent(searchTerm) + 
		"&format=" + "image";
	request.get(url, function(err, response, body) {
		if (err || response.statusCode != 200) {
			callback(new Error("Could not read WolframAlpha response."));
		} else {
			var xml = body;
			parseString(xml, function (err, result) {
    		if (err) {
    			callback(new Error("Could not parse XML."));
    		}
    		callback(err, result);
		});	
		}
		
  	});
};