var express = require('express');
var wolframalpha = require('./wolframalpha');

var app = express();

app.get('/wolframalpha/:queryTerm', function (req, res) {
  wolframalpha(req.params.queryTerm, function(err, data){
		if (err) {
			console.log(err);
		} else {
			res.send(parseWolframObject(data, req.params.queryTerm));
		}

  });
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});


function parseWolframObject(obj, term){
	switch(term){
		case "periodic table":
			return obj.queryresult.pod[1].subpod[0].img[0].$;
		case "alkali metals":
			return obj.queryresult.pod[2].subpod[0].img[0].$;
		default:
			return obj.queryresult.pod[1].subpod[0].img[0].$;;
	}
};